import React from "react";
import Styles from "./Hero.module.scss";
import NumberCard from "./NumberCard";
import AuctionLogo from "../../Assets/Pictures/AuctionLogo.png";
import ImageCard from "./ImageCard";
import Dot from "../../Assets/Svg/Dot.svg";

const Hero = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.containerLeft}>
                <h1 className={Styles.title}>
                    Discover, and collect <br /> Digital Art NFTs
                </h1>

                <article>
                    Digital marketplace for crypto collectibles and non-fungible
                    tokens (NFTs). Buy, Sell, and discover exclusive digital
                    assets.
                </article>
                <div className={Styles.leftInnerContainer}>
                    <img className={Styles.dotImg} src={Dot} alt="dotBackground" />
                    <button className={Styles.btn}>Explore Now</button>
                    <br />
                    <div className={Styles.numberContainer}>
                        <NumberCard number="98" txt="Artwork" />
                        <NumberCard number="12" txt="Auction" />
                        <NumberCard number="15" txt="Artist" />
                    </div>
                </div>
            </div>
            <div className={Styles.containerRight}>
                <img className={Styles.auctionLogo} src={AuctionLogo} alt="auctionLogo" />
                <div className={Styles.imgContainer}>
                    
                    <div className={Styles.imgContainer1}>

                    </div>
                    <div className={Styles.imgContainer2}>

                    </div>
                    <div className={Styles.innerContainer}>
                        <ImageCard/>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    );
};

export default Hero;
