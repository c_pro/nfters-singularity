import React from "react";
import Styles from "./NumberCard.module.scss";

const NumberCard = ({ number, txt }) => {
    return (
        <div className={Styles.main}>
            <div className={Styles.container}>
                <h1 className={Styles.title}>{number}K+</h1>
            </div>
            <p className={Styles.txt}>{txt}</p>
        </div>
    );
};

export default NumberCard;
