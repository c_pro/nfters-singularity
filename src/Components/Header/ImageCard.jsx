import React from "react";
import Styles from "./ImageCard.module.scss";
import Avatar from "../../Assets/avatar.png";
import { FaEthereum } from "react-icons/fa";

import Countdown from "react-countdown";

// Timeup component
const Completionist = () => <span>Time up!</span>;

// Renderer callback with condition
const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
        // Render a completed state
        return <Completionist />;
    } else {
        // Render a countdown
        return (
            <p className={Styles.time}>
                {hours}h {minutes}m {seconds}s
            </p>
        );
    }
};

const ImageCard = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.top}>
                <h2>Abstr Gradient NFT</h2>
                <div className={Styles.avatarContainer}>
                    <img className={Styles.avatar} src={Avatar} alt="" />
                    <p className={Styles.name}>Arkhan17</p>
                </div>
            </div>
            
            <div className={Styles.glassEffect}>
                <div className={Styles.left}>
                    <small>Current Bid</small>
                    <div className={Styles.leftInnerContainer}>
                        <FaEthereum />
                        <p className={Styles.price}>0.25 ETH</p>
                    </div>
                </div>
                <div className={Styles.right}>
                    <small>Ends in</small>
                    <Countdown date={Date.now() + 10000000000000} renderer={renderer} />
                </div>
            </div>
            
        </div>
    );
};

export default ImageCard;
