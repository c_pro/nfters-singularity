import React from "react";
import Styles from "./TopMenu.module.scss";
import Logo from "../../Assets/logo.png";
import Menu from "./Menu";
import useToggler from "../../Hooks/useToggler";
import { HiMenu, HiSearch } from "react-icons/hi";

const TopMenu = () => {
    const [showMenu, setShowMenu] = useToggler();

    return (
        <div className={Styles.main}>
            <div className={Styles.box}>
                <img className={Styles.logo} src={Logo} alt="logo" />
                <ul>
                    <li>Marketplace</li>
                    <li>Resource</li>
                    <li>About</li>
                </ul>
            </div>
            <div className={Styles.box}>
                <div className={Styles.inputContainer}>
                    <input type="text" placeholder="Search" />
                    <HiSearch />
                </div>
                <div className={Styles.btnContainer}>
                    <button className={Styles.uploadBtn}>Upload</button>
                    <button className={Styles.walletBtn}>Connect Wallet</button>
                </div>
            </div>

            <HiMenu
                style={showMenu ? { display: "none" } : {}}
                onClick={setShowMenu}
                className={Styles.icon}
            />
            <div style={showMenu ? {} : { display: "none" }}>
                <Menu closeCallBack={setShowMenu} />
            </div>
        </div>
    );
};

export default TopMenu;
