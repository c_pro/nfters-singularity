import React from "react";
import Styles from "./Menu.module.scss";
import { HiSearch } from "react-icons/hi";
import {CgClose} from "react-icons/cg";
const Menu = ({closeCallBack}) => {
    return (
        <div className={Styles.main}>
            <div className={Styles.container}>
                <CgClose className={Styles.icon} onClick={closeCallBack}/>
            </div> 
            <div className={Styles.menuContainer}>
                <div className={Styles.inputContainer}>
                    <input type="text" placeholder="Search" />
                    <HiSearch/>
                </div>
                <br />
                <br />
                <div className={Styles.btnContainer}>
                    <button className={Styles.uploadBtn}>Upload</button>
                    <button className={Styles.walletBtn}>Connect Wallet</button>
                </div>
                <div className={Styles.listContainer}>
                    <ul>
                        <li>Marketplace</li>
                        <hr className={Styles.hr}/>
                        <li>Resource</li>
                        <hr className={Styles.hr}/>
                        <li>About</li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Menu;
