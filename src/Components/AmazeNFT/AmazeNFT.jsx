import React from "react";
import Styles from "./AmazeNFT.module.scss";
import Icon1 from "../../Assets/card-tick.png";
import Icon2 from "../../Assets/chart-square.png";

const AmazeNFT = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.box}>
                <h2 className={Styles.title}>
                    The amazing NFT art of the world here
                </h2>
            </div>
            <div className={Styles.box}>
                <img className={Styles.icon} src={Icon1} alt="icon" />
                <div>
                    <p className={Styles.secondTitle}>Fast Transaction</p>
                    <p className={Styles.article}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam etiam viverra tellus imperdiet.
                    </p>
                </div>
            </div>
            <div className={Styles.box}>
                <img className={Styles.icon} src={Icon2} alt="icon" />
                <div>
                    <p className={Styles.secondTitle}>Fast Transaction</p>
                    <p className={Styles.article}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam etiam viverra tellus imperdiet.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default AmazeNFT;
