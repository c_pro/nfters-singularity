import React from "react";
import Styles from "./DiscoverCard.module.scss";
import { FaEthereum } from "react-icons/fa";
import Countdown from "react-countdown";
import Avatar from "../../Assets/avatar.png";
import Avatar1 from "../../Assets/avatar2.png";
import Avatar2 from "../../Assets/avatar3.png";
import Avatar3 from "../../Assets/avatar4.png";

// Timeup component
const Completionist = () => <span>Time up!</span>;

// Renderer callback with condition
const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
        // Render a completed state
        return <Completionist />;
    } else {
        // Render a countdown
        return (
            <p className={Styles.timeLeft}>
                {hours}h {minutes}m {seconds}s
            </p>
        );
    }
};

const DiscoverCard = ({ img, time, name, eth, left, img1 = {Avatar}, img2 = {Avatar2}, img3 = {Avatar1}, img4 = {Avatar3}}) => {
   
    return (
        <div className={Styles.main}>
            <div className={Styles.imgContainer}>
                <img className={Styles.img} src={ img} alt="pic" />
                <div className={Styles.avatars}>
                    <img className={Styles.img1} src={img1} alt="avatar" />
                    <img className={Styles.img2} src={img2} alt="avatar" />
                    <img className={Styles.img3} src={img3} alt="avatar" />
                    <img className={Styles.img4} src={img4} alt="avatar" />
                </div>
            </div>

            <h3 className={Styles.title}>{name}</h3>
            <div className={Styles.container}>
                <div className={Styles.priceContainer}>
                    <FaEthereum className={Styles.icon} />
                    <p className={Styles.price}>{eth} ETH</p>
                </div>
                <p className={Styles.quantity}>1 of {left} </p>
            </div>
            <br />
            <hr className={Styles.hr} />
            <div className={Styles.container}>
                <Countdown date={Date.now() + time} renderer={renderer} />
                <p className={Styles.bid}>Place a bid</p>
            </div>
        </div>
    );
};

export default DiscoverCard;
