import React from "react";
import Styles from "./Discover.module.scss";
import DiscoverCard from "./DiscoverCard";
import { BsFilter } from "react-icons/bs";

import { Data } from "./DiscoverData";

const Discover = () => {
    const dataHandler = Data.map((singleData, index) => {
        return (
            <DiscoverCard
                key={index}
                img={singleData.img}
                time={singleData.timeLeft}
                name={singleData.title}
                eth={singleData.eth}
                left={singleData.left}
                img1={`https://picsum.photos/100/100?random=${Math.floor(Math.random() * 100)}`}
                img2={`https://picsum.photos/100/100?random=${Math.floor(Math.random() * 100)}`}
                img3={`https://picsum.photos/100/100?random=${Math.floor(Math.random() * 100)}`}
                img4={`https://picsum.photos/100/100?random=${Math.floor(Math.random() * 100)}`}
            />
        );
    });
    return (
        <div className={Styles.main}>
            <h2 className={Styles.title}>Discover more NFTs</h2>
            <br />
            <br />
            <br />
            <div className={Styles.menuContainer}>
                <div className={Styles.left}>
                    <p className={Styles.menu}>All Categories</p>
                    <p className={Styles.menu}>Art</p>
                    <p className={Styles.menu}>Celebrities</p>
                    <p className={Styles.menu}>Gaming</p>
                    <p className={Styles.menu}>Sports</p>
                    <p className={Styles.menu}>Music</p>
                    <p className={Styles.menu}>Crypto</p>
                </div>
                <div className={Styles.right}>
                    <BsFilter />
                    <p>Filter</p>
                </div>
            </div>
            <div className={Styles.container}>
                {dataHandler}
            </div>
            <div className={Styles.btnContainer}>
                <button className={Styles.moreBtn}>More NFTs</button>
            </div>
        </div>
    );
};

export default Discover;
