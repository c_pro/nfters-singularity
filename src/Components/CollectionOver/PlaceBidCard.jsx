import React from 'react';
import Styles from "./PlaceBidCard.module.scss";
import Img from "../../Assets/Pictures/pic6.png";
import Avatar from "../../Assets/avatar.png";
import { FaEthereum } from "react-icons/fa";

const PlaceBidCard = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.imgBox}>
                <img src={Img} alt="pic" />
            </div>
            <div className={Styles.txtBox}>
                <h3>The Futr Abstr</h3>
                <div className={Styles.container}>
                    <img src={Avatar} alt="avatar" />
                    <div className={Styles.ether}>
                        <FaEthereum/>
                        <p>0.25 ETH</p>
                    </div>
                    <small>1 of 12</small>
                </div>
                <button className={Styles.btn}>Place a bid</button>
            </div>
        </div>
    );
};

export default PlaceBidCard;