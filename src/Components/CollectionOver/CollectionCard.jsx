import React from "react";
import Styles from "./CollectionCard.module.scss";
import Avatar from "../../Assets/avatar2.png";
import Verify from "../../Assets/verify.png";
import { FaEthereum } from "react-icons/fa";

const CollectionCard = ({sr, profit = true, verify = false}) => {
    return (
        <div className={Styles.main}>
            <div className={Styles.section}>
                <h3>{sr}</h3>
            </div>
            <div className={Styles.section}>
                <div className={Styles.imgContainer}>
                    <img style={!verify ? {display: "none"} :{}} className={Styles.verify} src={Verify} alt="" />
                    <img className={Styles.avatar} src={Avatar} alt="" />
                </div>
                
            </div>
            <div className={Styles.section}>
                <p>CryptoFunks</p>
                <div className={Styles.ether}>
                    <FaEthereum />
                    <small>0.25 ETH</small>
                </div>
            </div>
            <div className={Styles.section}>
                <h3 style={profit ? {color: "#00AC4F"} : {color: "#ff002e"}}>+26.52%</h3>
            </div>
        </div>
    );
};

export default CollectionCard;
