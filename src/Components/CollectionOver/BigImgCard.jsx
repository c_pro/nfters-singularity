import React from "react";
import Styles from "./BigImgCard.module.scss";
import Img from "../../Assets/Pictures/pic1.png";
import Avatar from "../../Assets/avatar.png";
import { FaEthereum } from "react-icons/fa";

const BigImgCard = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.wrapper}>
                <img className={Styles.img} src={Img} alt="pic" />
            </div>
            
            <br />
            <div className={Styles.container}>
                <div className={Styles.avatarContainer}>
                    <img className={Styles.avatar} src={Avatar} alt="avatar" />
                    <div>
                        <p>The Futr Abstr</p>
                        <small>10 in the stock</small>
                    </div>
                </div>
                <div className={Styles.bidContainer}>
                    <small>Highest Bid</small>
                    <div>
                        <FaEthereum/>
                        <p>0.25 ETH</p>
                    </div>
                    
                </div>
            </div>
        </div>
    );
};

export default BigImgCard;
