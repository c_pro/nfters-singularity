import React from "react";
import BigImgCard from "./BigImgCard";
import CollectionCard from "./CollectionCard";
import Styles from "./CollectionOver.module.scss";
import PlaceBidCard from "./PlaceBidCard";

const CollectionOver = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.box}>
                <BigImgCard />
            </div>
            <div className={Styles.box}>
                <PlaceBidCard />
                <PlaceBidCard />
                <PlaceBidCard />
            </div>
            <div className={Styles.box}>
                <div className={Styles.wrapper}>
                    <div className={Styles.vl}></div>
                    <div className={Styles.innerContainer}>
                        <h3 className={Styles.title}>Top Collections over</h3>
                        <p className={Styles.txt}>Last 7 days</p>
                        <CollectionCard sr="1" verify={true} />
                        <hr />
                        <CollectionCard sr="2" profit={false} />
                        <hr />
                        <CollectionCard sr="3" verify={true} />
                        <hr />
                        <CollectionCard sr="4"  />
                        <hr />
                        {/* <CollectionCard sr="5" profit={false} />
                        <hr /> */}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CollectionOver;
