import React from "react";
import Styles from "./SignUp.module.scss";
import Img1 from "../../Assets/Pictures/pic5.png";
import Img2 from "../../Assets/Pictures/pic6.png";
import Img3 from "../../Assets/Pictures/pic1.png";
import ImgCard from "./ImgCard";

import Avatar from "../../Assets/avatar.png";

const SignUp = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.containerLeft}>
                <div className={Styles.column}>
                    <ImgCard
                        height="clamp(150px, 20vw, 300px)"
                        width="clamp(150px, 20vw, 300px)"
                        avatar={Avatar}
                        img={Img1}
                    />
                    <ImgCard
                        height="clamp(110px, 15vw, 270px)"
                        width="clamp(90px, 15vw, 240px)"
                        avatar={Avatar}
                        img={Img2}
                    />
                </div>
                <ImgCard
                    height="clamp(120px, 17vw, 270px)"
                    width="clamp(90px, 17vw, 240px)"
                    avatar={Avatar}
                    img={Img3}
                />
            </div>
            <div className={Styles.containerRight}>
                <h2 className={Styles.title}>Create and sell your NFTs</h2>
                <br />
                <article>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Facilisi ac phasellus placerat a pellentesque tellus sed
                    egestas. Et tristique dictum sit tristique sed non. Lacinia
                    lorem id consectetur pretium diam ut. Pellentesque eu sit
                    blandit fringilla risus faucibus.
                </article>
                <br />
                <button className={Styles.btn}>Sign Up Now</button>
            </div>
        </div>
    );
};

export default SignUp;
