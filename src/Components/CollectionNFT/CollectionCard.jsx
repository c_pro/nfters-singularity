import React from "react";
import Styles from "./CollectionCard.module.scss";


const CollectionCard = ({Pic1,Pic2,Pic3,Pic4, avatar, itemNo, name }) => {
    return (
        <div className={Styles.main}>
            <div className={Styles.imgContainer}>
                <div className={Styles.bigImgContainer}>
                    <img className={Styles.bigImg} src={Pic1} alt="pic" />
                </div>
                <div className={Styles.smlImgContainer}>
                    <img className={Styles.smlImg} src={Pic2} alt="pic" />
                    <img className={Styles.smlImg} src={Pic3} alt="pic" />
                    <img className={Styles.smlImg} src={Pic4} alt="pic" />
                </div>
            </div>
            <br />
            <div className={Styles.detailsContainer}>
                <h3>Amazing Collection</h3>
                <br />
                <div className={Styles.box}>
                    <div className={Styles.avatarContainer}>
                        <img
                            className={Styles.avatar}
                            src={avatar}
                            alt="avatar"
                        />
                        <h4>by {name}</h4>
                    </div>
                    <button className={Styles.btn}>Total {itemNo} items</button>
                </div>
                <br />
            </div>
        </div>
    );
};

export default CollectionCard;
