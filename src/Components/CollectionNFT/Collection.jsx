import React from 'react';
import Styles from "./Collection.module.scss";
import CollectionCard from './CollectionCard';
import Pic1 from "../../Assets/Pictures/pic1.png";
import Pic2 from "../../Assets/Pictures/pic2.png";
import Pic3 from "../../Assets/Pictures/pic3.png";
import Pic4 from "../../Assets/Pictures/pic4.png";
import Pic5 from "../../Assets/Pictures/pic5.png";
import Pic6 from "../../Assets/Pictures/pic6.png";
import Avatar from "../../Assets/avatar.png";

const Collection = () => {
    return (
        <div className={Styles.main}>
            <h2 className={Styles.title}>Collection Featured NFTs</h2>
            <br /><br /><br />
            <div className={Styles.collectionContainer}>
                <CollectionCard Pic1={Pic1} Pic2={Pic2} Pic3={Pic3} Pic4={Pic4} name="Akhkhan" avatar={Avatar} itemNo="54"/>
                <CollectionCard Pic1={Pic5} Pic2={Pic3} Pic3={Pic4} Pic4={Pic1} name="Akhkhan" avatar={Avatar} itemNo="54"/>
                <CollectionCard Pic1={Pic6} Pic2={Pic4} Pic3={Pic1} Pic4={Pic2} name="Akhkhan" avatar={Avatar} itemNo="54"/>
            </div>
        </div>
    );
};

export default Collection;