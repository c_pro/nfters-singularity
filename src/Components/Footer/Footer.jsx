import React from "react";
import Styles from "./Footer.module.scss";
import { FaFacebook, FaTwitter, FaLinkedin } from "react-icons/fa";

const Footer = () => {
    return (
        <div className={Styles.main}>
            <div className={Styles.upperSection}>
                <section>
                    <h2 className={Styles.title}>NFters</h2>
                    <br />
                    <article>
                        The world’s first and largest digital marketplace for
                        crypto collectibles and non-fungible tokens (NFTs). Buy,
                        sell, and discover exclusive digital items.
                    </article>
                    <div className={Styles.social}>
                        <FaFacebook className={Styles.fb} />
                        <FaTwitter className={Styles.tw} />
                        <FaLinkedin className={Styles.li} />
                    </div>
                </section>
                <section>
                    <div className={Styles.menuContainer}>
                        <ul>
                            <li className={Styles.menuTitle}>Market Place</li>
                            <li>All NFTs</li>
                            <li>New</li>
                            <li>Art</li>
                            <li>Sport</li>
                            <li>Utility</li>
                            <li>Music</li>
                            <li>Domain Name</li>
                        </ul>
                        <ul>
                            <li className={Styles.menuTitle}>My Account</li>
                            <li>Profile</li>
                            <li>Favorite</li>
                            <li>My Collections</li>
                            <li>Settings</li>
                        </ul>
                    </div>
                </section>
                <section>
                    <p className={Styles.stayTitle}>Stay in the loop</p>
                    <article>
                        Join our mailing list to stay in the loop with our
                        newest feature releases, NFT drops, and tips and tricks
                        for navigating NFTs.
                    </article>
                    <div className={Styles.inputContainer}>
                        <input
                            type="text"
                            placeholder="Enter your email address.."
                        />
                        <button>Subscribe Now</button>
                    </div>
                </section>
            </div>
            <br />
            <br />
            <br />
            <div className={Styles.lowerSection}>
                <hr className={Styles.hr}/>
                <br />
                <p className={Styles.copyright}>Copyright &#169; {new Date().getFullYear()} Singularity; design by Avi Yansah</p>
            </div>
        </div>
    );
};

export default Footer;
