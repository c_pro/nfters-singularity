import React from 'react';
import AmazeNFT from '../Components/AmazeNFT/AmazeNFT';
import Collection from '../Components/CollectionNFT/Collection';
import CollectionOver from '../Components/CollectionOver/CollectionOver';
import Discover from '../Components/Discover/Discover';
import Footer from '../Components/Footer/Footer';
import Hero from '../Components/Header/Hero';
import TopMenu from '../Components/Header/TopMenu';
import SignUp from '../Components/SignUp/SignUp';
import Styles from "./Home.module.scss";

const Home = () => {
    return (
        <div className={Styles.main}>
            <TopMenu/>
            <Hero/>
            <AmazeNFT/>
            <CollectionOver/>
            <Collection/>
            <SignUp/>
            <Discover/>
            <Footer/>
        </div>
    );
};

export default Home;